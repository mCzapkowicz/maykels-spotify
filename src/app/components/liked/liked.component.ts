import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';
import {ServerService} from "../../services/server.service";
import {ToastsManager} from "ng2-toastr";
import * as FileSaver from 'file-saver';

@Component({
  templateUrl: '../../components/liked/liked.component.html',
  styleUrls: ['./liked.styles.css']
})

export class LikedComponent  {

  private id : string;
  private likes: any;
  previewTrack: boolean;
  private trackId: string;

  constructor(private spotifyService: SpotifyService, private route: ActivatedRoute, private serverService: ServerService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);

    this.id = sessionStorage.getItem('id');
    console.log("id", this.id);

    serverService.getLikes(this.id).subscribe( (data) =>
      {
        this.likes = data;
        for(let i = 0; i<this.likes.length; i++) {
          spotifyService.getSongDetails(this.likes[i].spotify_id).subscribe( (data) => {
            this.likes[i].spotify_info = data;
            console.log(this.likes[i]);
          });
        }

        console.log(this.likes);
      });


  }

  selectTrack(trackId: string) {
    if(this.trackId == trackId) {
      this.trackId = '';
    }
    else{
      this.trackId = trackId;
    }

  }

  isSelected(track: string){
    if(track == this.trackId) {
      return true;
    }
    else {
      return false;
    }
  }

  deleteFromFavourites(likeObj) {
    this.serverService.deleteFromFavourites(likeObj).subscribe((data)=> {
      this.toastr.info("Has been removed!", likeObj.name);
      this.removeFromTable(likeObj);
      console.log(data);
    })
  }

  removeFromTable(item) {
    let index = this.likes.findIndex(x => x.spotify_id == item.spotify_id);
    if(index > -1) {
      this.likes.splice(index,1);
    }
  }

  downloadSong(url, title){
    var filename= title+'.mp3';
    this.spotifyService.download(url).then(function(data){
        let blob = new Blob([data], { type: 'audio/mpeg' });
        FileSaver.saveAs(blob, filename);
      }
    )
  }
}
