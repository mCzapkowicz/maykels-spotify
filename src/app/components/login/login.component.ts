import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ServerService} from "../../services/server.service";
import {ToastsManager} from "ng2-toastr";

@Component({
  templateUrl: '../../components/login/login.component.html',
  styleUrls: ['./login.styles.css'],
})


export class LoginComponent{
  loginForm: FormGroup;
  registerForm: FormGroup;
  login: string;
  password: string;
  constructor( private route: ActivatedRoute, private fb: FormBuilder, private serverService: ServerService,
               public toastr: ToastsManager, vcr: ViewContainerRef, private router: Router) {

    this.toastr.setRootViewContainerRef(vcr);


    this.loginForm = fb.group({
      "login":["",Validators.required],
      "password":["", Validators.required]
    });

    this.registerForm = fb.group({
      "login":["",Validators.required],
      "password":["", Validators.required],
      "name":["", Validators.required],
      "surname":["", Validators.required]
    });

  }

  onLogin(formData) {
    console.log("model-based form submitted", formData.value);
    this.serverService.logIn(formData.value).subscribe(
      (res)=> {
        console.log('res', res.id);

        this.toastr.success("Nice to see you", 'Hello '+formData.value.login+'!');

        sessionStorage.setItem('login',formData.value.login);
        sessionStorage.setItem('id', res.id);
        var that = this;

        setTimeout(
          function() {
            that.router.navigate(['/search']);
          }, 2000);

        console.log(sessionStorage.getItem('login'));

        this.serverService.getLikes(sessionStorage.getItem('id')).subscribe( (data) => {
          console.log('likes', data);
        });
        this.serverService.updateNavbarCounter();

      },
      (err)=>{
        this.toastr.error('No such user or password', 'Error!');
        console.log('err', err)
      }
    );
  }

  onRegister(form) {
    console.log('registet values', form.value);
    this.serverService.register(form.value).subscribe(
      (res) =>
      {
        var that = this;
        setTimeout(
          function() {
            location.reload();
          }, 2000);
        this.toastr.success('Account created!', 'Success!');

      },
      (err) =>{
        console.log('err',err);
      });
  }

  ngAfterViewInit(){
   $('.message a').click(function(event){
      event.preventDefault();
      $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
      return false;
    });
  }
}
