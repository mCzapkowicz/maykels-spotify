import {Component, OnInit, ViewContainerRef} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SpotifyService } from '../../services/spotify.service';
import { Album } from '../../models/Album'
import * as FileSaver from 'file-saver';
import {ServerService} from "../../services/server.service";
import {ToastsManager} from "ng2-toastr";

@Component({
  templateUrl: '../../components/album/album.component.html',
})

export class AlbumComponent {

  id: string;
  previewTrack: boolean;
  private trackId: string;

  album: Album[];

  constructor(private spotifyService: SpotifyService, private route: ActivatedRoute, private serverService: ServerService, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.previewTrack = false;
    this.toastr.setRootViewContainerRef(vcr);

  }


  ngOnInit() {
    this.route.params
      .map( params => params['id'])
      .subscribe( (id) => {
        this.spotifyService.getAlbum(id)
          .subscribe( (album)=> {
            this.album = album;
            console.log('album', album);

          })
      })


  }

  selectTrack(trackId: string) {
    if(this.trackId == trackId) {
      this.trackId = '';
    }
    else{
      this.trackId = trackId;

    }

  }

  isSelected(track: string){
    if(track == this.trackId) {
      return true;
    }
    else {
      return false;
    }
  }


  addToFavourites(track: any) {
    var likedObj = {
      'spotify_id' : track.id,
      'name' : track.name,
      'artist' : track.artists[0].name,
      'user_id' : sessionStorage.getItem('id')
    };
    this.serverService.addToFavourites(likedObj).subscribe( (data)=> {
      this.toastr.success("Added to your favourites", track.name);
    });
    console.log('liked', likedObj);
  }

  downloadSong(url, title){
      var filename= title+'.mp3';
      this.spotifyService.download(url).then(function(data){
          let blob = new Blob([data], { type: 'audio/mpeg' });
          FileSaver.saveAs(blob, filename);
          }
      )
  }

}
