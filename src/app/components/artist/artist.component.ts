import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


import { SpotifyService } from '../../services/spotify.service';

import { Artist } from '../../models/Artist'
import { Album } from '../../models/Album'

@Component({
  templateUrl: '../../components/artist/artist.component.html',
})

export class ArtistComponent implements OnInit {

  id: string;
  artist: Artist[];
  albums: Album[];

  constructor(private spotifyService: SpotifyService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.route.params
      .map( params => params['id'])
      .subscribe( (id) => {
        this.spotifyService.getArtist(id)
          .subscribe( (artist)=> {this.artist = artist; })

        this.spotifyService.getAlbums(id)
          .subscribe( (albums)=> {
            console.log('albums',albums);
            //this.albums = albums.items;
            this.albums = this.removeDuplicates(albums.items,'name');
          })
      })


  }
  removeDuplicates(array, property) {
      let newArray = [];
      let lookup = {};

      for(let i in array) {
        lookup[ array[i][property] ] = array[i];
      }

      for(let i in lookup) {
        newArray.push(lookup[i]);
      }

      return newArray;
  }
}
