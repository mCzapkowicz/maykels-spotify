import {Component, ViewContainerRef} from '@angular/core';
import {ToastsManager} from "ng2-toastr";
import {ServerService} from "../../services/server.service";

@Component({
  selector: 'navbar',
  templateUrl: '../../components/navbar/navbar.component.html',
})

export class NavbarComponent {

  private id: string;
  private likesAmount: number = 0;
  subscription: any;

  constructor(public toastr: ToastsManager, vcr: ViewContainerRef,private serverService: ServerService) {
    this.toastr.setRootViewContainerRef(vcr);
    this.id = sessionStorage.getItem('id');

    this.getLikesAmount();
  }

  ngOnInit() {
    this.subscription = this.serverService.likesChanged
      .subscribe(item => {
        this.refreshCounter();
      });
  }

  isLogged() {
    if(sessionStorage.getItem('login')) {
      return true;
    } else {
      return false;
    }
  }

  logOut() {
    sessionStorage.removeItem('login');
    sessionStorage.removeItem('id');

    this.toastr.success('You have logged out!', 'Success!');
  }

  getLikesAmount() {

    this.serverService.getLikes(this.id).subscribe( (data) =>
    {
      this.likesAmount = data.length;
    });
  }

  refreshCounter() {
    console.log("Refreshing!");
    this.id = sessionStorage.getItem('id');
    this.getLikesAmount();
  }
}
