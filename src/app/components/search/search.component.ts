import { Component, ViewContainerRef} from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../models/Artist';
//import { Translation, LocaleService, TranslationService } from 'angular-l10n';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { ServerService } from '../../services/server.service';
import { AnimationBuilder } from 'css-animator/builder';

@Component({
  selector: 'search',
  templateUrl: '../../components/search/search.component.html'
})

export class SearchComponent  {

  searchStr: string;
  searchRes: Artist[];
  items : any;
  posts: any = [];
  constructor(private serverService: ServerService, private spotifyService: SpotifyService, public toastr: ToastsManager, vcr: ViewContainerRef) {
      this.toastr.setRootViewContainerRef(vcr);
  }
  searchMusic() {
    this.spotifyService.searchMusic(this.searchStr)
      .subscribe( res => { console.log( this.searchRes = res.artists.items ) }
    );
  }

}
