import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { routing } from './app.routes'; // ROUTING HERE!

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { AboutComponent } from './components/about/about.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistComponent } from './components/artist/artist.component';
import { AlbumComponent } from './components/album/album.component';
import { TranslationModule } from 'angular-l10n';
//import { AngularFireModule } from 'angularfire2';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { ServerService } from './services/server.service';
import {LoginComponent} from "./components/login/login.component";
import 'jquery';
import {LikedComponent} from "./components/liked/liked.component";

export const firebaseConfig = {
   apiKey: "AIzaSyAyPOXqWT_phmcZzU4wMuGH6aiK1xm5cy0",
   authDomain: "maykels-spotify.firebaseapp.com",
   databaseURL: "https://maykels-spotify.firebaseio.com",
   storageBucket: "maykels-spotify.appspot.com",
   messagingSenderId: "123405074973"
 };

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AboutComponent,
    SearchComponent,
    ArtistComponent,
    AlbumComponent,
    LoginComponent,
    LikedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    //AngularFireModule.initializeApp(firebaseConfig),
    ToastModule.forRoot()
    //TranslationModule.forRoot()
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule {

}
