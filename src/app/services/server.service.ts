import {Injectable, Output, EventEmitter} from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class ServerService {
  @Output() likesChanged: EventEmitter<any> = new EventEmitter();
  constructor(private http: Http) {

  }

  logIn(loginObj: any) {
    this.likesChanged.emit('logged');
    return this.http.post('/api/logIn',loginObj).map( res => res.json() );
  }

  register(registerObj: any) {
    return this.http.post('/api/register', registerObj).map( res => res.json() );
  };

  addToFavourites(likeObj: any) {
    return this.http.post('/api/likes', likeObj).map(
      res => {
        res.json();
        this.likesChanged.emit('song added to favourites');

      }
    );
  }

  getLikes(user_id: string) {
    let id ={'user_id': user_id};
    console.log('Getting likes for user: ', user_id);
    return this.http.get('api/likes/'+user_id)
      .map( res=> res.json() );
  }

  deleteFromFavourites(likeObj) {
    let user_id = likeObj.user_id;
    let spotify_id = likeObj.spotify_id;

    return this.http.delete('api/likes/' + user_id + '/' + spotify_id)
      .map( res=> {
        res.json();
        this.likesChanged.emit('song deleted');
      } );
  }

  updateNavbarCounter() {
    this.likesChanged.emit('song added');
  }
}
