import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {
  private searchUrl;
  private artistUrl;
  private albumsUrl;
  private albumUrl;

  constructor(private http: Http) {

  }

  searchMusic(str: string, type='artist') {
    this.searchUrl = 'https://api.spotify.com/v1/search?query='+str+'&offset=0&limit=20&type='+type+'&market=US';
    return this.http.get(this.searchUrl).map(res => res.json());
  }

  getArtist(id: string) {
    this.artistUrl = 'https://api.spotify.com/v1/artists/'+id;
    return this.http.get(this.artistUrl).map(res => res.json());
  }

  getAlbums(artistId: string) {
    this.albumsUrl = 'https://api.spotify.com/v1/artists/'+artistId+'/albums';
    return this.http.get(this.albumsUrl).map(res => res.json());
  }

  getAlbum(id: string) {
    console.log('album', id);

    this.albumUrl = 'https://api.spotify.com/v1/albums/'+id;
    return this.http.get(this.albumUrl).map(res => res.json());
  }

  getSongDetails(song_id: string) {
    var url = 'https://api.spotify.com/v1/tracks';
    return this.http.get(url+'/'+song_id)
      .map(res => res.json());
  }
  // download(link: string) {
  //     console.log('downolad', link);
  //     var url = link;
  //     return this.http.get(url);
  // }
  download(url: string): Promise<any> {
      console.log('download2');
      return new Promise((resolve, reject) => {
          let xhr = new XMLHttpRequest();
          xhr.open('GET', url, true);
          xhr.responseType = 'blob';
          xhr.onload = function () {
              if (xhr.readyState === 4 && xhr.status === 200) {
                  resolve(xhr.response);
              }
              else {
                  reject({
                      status: xhr.status,
                      statusText: xhr.statusText
                  });
              }
          };
          xhr.send()
      });
  }

  test(){
      return this.http.get('https://maykels-spotify.firebaseio.com/.json').map(
          (res)=> res.json()
      );

  }
}
