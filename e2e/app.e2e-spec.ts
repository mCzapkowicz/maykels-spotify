import { MaykelSpotifyPage } from './app.po';

describe('maykel-spotify App', function() {
  let page: MaykelSpotifyPage;

  beforeEach(() => {
    page = new MaykelSpotifyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
