const express = require('express');
const router = express.Router();
var pg = require('pg');
var query = require('../queries/queries');
var jwt = require('jsonwebtoken');


//Db connection string
var conString = 'postgres://qxeiolzwrugglq:1940607108ff2fc9a94b3fb50b9e903b8d975f97259326e078ed160b7d98d549@ec2-54-247-81-76.eu-west-1.compute.amazonaws.com:5432/d1balsp44042q3?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory';

pg.connect(conString,function(err, client, done) {

  if(err) {
    return console.error('error fetching client from pool', err);
  }
  else{
    console.log('Connected to db');
  }
});


/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.post('/logIn', (req, res) => {
  var login = req.body.login;
  var password = req.body.password;
  query.logIn(login,password,function(data) {
    if(data.length > 0) {
      res.status(200).json( {id: data[0].id });
    }
    else{
      res.status(404).send({message: 'No such user'})
    }
  });
  console.log('request', login, password);
});



router.post('/likes', (req, res) => {
  console.log('got request!');

  var user_id = req.body.user_id;
  var spotify_id = req.body.spotify_id;
  var name = req.body.name;
  var artist = req.body.artist;

  query.addToFavourites(spotify_id, name, artist, user_id, function(data) {
    if(data) {
      console.log(data);
      res.status(200).json({message: 'ok'});
    }
  });
});

router.get('/likes/:user_id', (req, res) => {

  console.log('got request for get likes!');
  var user_id = req.params.user_id;
  console.log('user: ', user_id);
  query.getLikes(user_id, function(data) {
    if(data) {
      res.status(200).send(data.rows);
    }
  });
});

router.delete('/likes/:user_id/:spotify_id', (req, res) => {
  let user_id = req.params.user_id;
  let spotify_id = req.params.spotify_id;
  console.log('got request for delete likes!');

  console.log('user: ', user_id);
  query.deleteFromFavourites(user_id,spotify_id, function(data) {
    if(data) {
      res.status(200).json({status: 'ok'});
    }
  });
});

router.post('/register', (req, res) => {
  var login = req.body.login;
  var password = req.body.password;
  var name = req.body.name;
  var surname = req.body.surname;
  query.register(login,password,name, surname, function(data) {
    if(data) {
      res.status(200).json({login: login});
    }
  });
});

router.get('/posts', (req, res) => {
    var testObj = {nazwa: 'dupinka'}
    res.status(200).json(testObj);
});

module.exports = router;
