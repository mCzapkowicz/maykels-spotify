var pg = require('pg');
var conString = 'postgres://qxeiolzwrugglq:1940607108ff2fc9a94b3fb50b9e903b8d975f97259326e078ed160b7d98d549@ec2-54-247-81-76.eu-west-1.compute.amazonaws.com:5432/d1balsp44042q3?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory';

module.exports = {

  logIn: function(login,password,callback)
  {
    pg.connect(conString,function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      var q = "select id from users where login = '"+login+"' and password= '"+password+"'";
      client.query(q, function(err,result){
        callback(result.rows);
        done();
      });
    });
  },

  register: function(login,password,name,surname, callback)
  {
    pg.connect(conString,function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var q =  "insert into users(login,password,name,surname) values ('"+login+"','"+password+"','"+name+"','"+surname+"')";
      client.query(q, function(err,result){
        callback(result);
        done();
      });
    });
  },

  addToFavourites: function(spotify_id,name,artist,user_id, callback)
  {
    console.log('adding to favouroites');

    pg.connect(conString,function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var q =  "insert into liked(spotify_id,name,artist,user_id) values ('"+spotify_id+"','"+name+"','"+artist+"','"+user_id+"')";
      client.query(q, function(err,result){
        callback(result);
        done();
      });
    });
  },

  getLikes: function(user_id, callback)
  {
    console.log('adding to favouroites');

    pg.connect(conString,function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var q =  "select * from liked where user_id =" +user_id;
      client.query(q, function(err,result){
        if(err) console.log(err);
        else{
          callback(result);
        }
        done();


      });
    });
  },

  deleteFromFavourites: function(user_id,spotify_id, callback)
  {
    console.log('deleteing');

    pg.connect(conString,function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var q =  "delete from liked where user_id =" + user_id + " and spotify_id = '" + spotify_id+"'";
      console.log('query: ', q);
      client.query(q, function(err,result){
        if(err) console.log(err);
        else{
          callback(result);
        }
        done();


      });
    });
  }
};
